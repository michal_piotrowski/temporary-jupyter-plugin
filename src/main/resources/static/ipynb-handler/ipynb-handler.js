console.log('IPYNB Handler loading! update!');

define('ipynb/ipynb-handler', [
    'jquery'
], function(
    $
) {
    "use strict";

    var ipynbViewResourceKey = 'com.atlassian.bitbucket.server.plugin.bitbucket-ipynb-plugin:ipynb-view';

    /**
     * Extract the extension from file name
     * @param {Object} filePath - the file path object
     * @returns {string} extension of file
     */
    function getFileExtension(filePath) {
        if (filePath.extension) {
            return filePath.extension;
        }
        var splitName = filePath.name.split('.');
        return splitName[splitName.length - 1];
    }

    /**
     * @returns Raw url for file
     */
    function getRawUrl(fileChange) {
        var projectKey = fileChange.repository.project.key;
        var repoSlug = fileChange.repository.slug;
        var path = fileChange.path.components.join('/');
        var revisionId = fileChange.commitRange.untilRevision.id;

        return AJS.contextPath() + AJS.format('/projects/{0}/repos/{1}/browse/{2}?at={3}&raw',
            projectKey, repoSlug, path, encodeURIComponent(revisionId));
    }

    // Register a file-handler for .ipynb files
    return function(options) {
        var fileChange = options.fileChange;

        // Check if .ipynb file
        var isIPYNB = getFileExtension(fileChange.path) === 'ipynb';

        if (isIPYNB && options.contentMode === 'diff') {
            // Not implemented, yet ;) TODO

        } else if (isIPYNB && options.contentMode === 'source') {
            // Asynchronously load ipynb-view web-resources (js/css/soy)
            var deferred = new $.Deferred();

            WRM.require('wr!' + ipynbViewResourceKey).done(function() {
                // When web-resources successfully loaded, create a IPYNBView
                var IPYNBView = require('ipynb/ipynb-view');
                var fileUrl = getRawUrl(fileChange);
                var view = new IPYNBView(options.$container, fileUrl);

                // This class gets added to the file-content
                view.extraClasses = 'ipynb-file';

                deferred.resolve(view);
            }).fail(function() {
                /* Something went wrong trying to load the web-resources asynchronously,
                 and therefore we can't handle the file so reject the promise */
                console.log('error while asynchronously loading ipynb-view resources');
                return deferred.reject();
            });
            
            return deferred;
        }

        // We can't handle this file
        return false;
    }
});


require('bitbucket/feature/files/file-handlers').register({
    weight: 900,
    handle: function(options) {
        return require('ipynb/ipynb-handler').apply(this, arguments);
    }
});

