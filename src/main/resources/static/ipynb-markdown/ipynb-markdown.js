define('ipynb/ipynb-markdown', [ 'jquery' ], function( $ ) {
    $(function () {
        
        var loc = window.location.pathname.search(/\/browse/);
        var url_prefix = window.location.pathname.substring(0, loc + 7) + '/';
        
        setTimeout(function() {
            jQuery(".markup-content.markup > p").each(function(index, element) {
                
                var match = /^ipynb\((.*)\)$/.exec(element.innerHTML.trim());
                if (match !== null) {
                    var arguments = match[1].split(/,/);
                    var decode_element = document.createElement('textarea');
                    decode_element.innerHTML = arguments[0];
                    var file = url_prefix + decode_element.value;
                    
                    var cell = parseInt(arguments[1]);
                    arguments = arguments.splice(2);
                    $.get(file, function(element, cell, arguments, data) {
                        var ipynb_json = JSON.parse(data);
                        ipynb_json['cells'] = [ipynb_json['cells'][cell]];
                        var html = nb.parse(ipynb_json).render().outerHTML;
                        var ipynb_element = $(html);
                        if (arguments.indexOf("no-inputs") >= 0)
                            $(".nb-input", ipynb_element).remove();
                        if (arguments.indexOf("no-outputs") >= 0)
                            $(".nb-output", ipynb_element).remove();
                        
                        $(element).replaceWith(ipynb_element);
                        Prism.highlightAll();
                    }.bind(null, element, cell, arguments));
                }

            });
        }, 2000); // ugly, yet effective.
    });
});

require('ipynb/ipynb-markdown');