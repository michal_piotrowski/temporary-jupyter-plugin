IPython Notebook Plugin (for Bitbucket Server)
=========================


A plugin for [Atlassian Bitbucket Server](https://www.atlassian.com/software/stash/overview) to render .ipynb files
in the browser when viewing their source. Rendering the diffs of the notebooks is not part of the library.

Created by Herbert Kruitbosch to improve documentation and illustration of experiments as run by the Target
Holding Artificial intelligence team.

### Compatibility ###

Requires Atlassian Bitbucket 4.0 or higher

### Dependencies (included in the source) ###

* notebook.js
* ansi_up.js
* marked.js
* prism.js


### Source ###

Hosted on [Atlassian Bitbucket](https://bitbucket.org/hkruitbosch/stash-ipynb-plugin).

Clone from:

```
git@bitbucket.org:hkruitbosch/stash-ipynb-plugin.git
```

### Development ###

This is an Atlassian plugin so you will need the
[Atlassian SDK installed](https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK).

To run the bitbucket-server, execute this command in the repository root:

`atlas-run --server 0.0.0.0 --http-port 8002 --product bitbucket --container tomcat8x`

To install the add-on/package/plugin/whatever into this development server, run in the repository root:

`atlas-package`

`atlas-install-plugin --http-port 8002`

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
