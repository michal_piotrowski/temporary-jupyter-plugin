# Bitbucket IPython Notebook Plugin Privacy Policy

To the best of our knowledge, we do not collect any of your personal data, however we do provide this
plugin "AS IS". In principle we only convert your ipynb-files in the clients browser, and no data gets
to any server, in fact, the developers of this plugin did not provide any server for it to function.
However, we do use third party JavaScript libraries, that are also injected. We did not check those source codes,
please do that yourself if worried. Moreover, there can be bugs, and none of the plugin's developers are security
experts. You can find the source code here: [source code](https://bitbucket.org/hkruitbosch/bitbucket-ipynb-plugin/)

Since you can fork the source code and create your own version of it, you can not hold us, the developers, accountable
for damage caused by it in any way. Deployer be aware. If your source code is precious, it's up to you to make sure
this plugin is thoroughly reviewed before use. We are very eager to get feedback, such that we can either fix issues
or document them such that the community can fix them. Pull requests are welcome.

Please let us know if you find anything strange.